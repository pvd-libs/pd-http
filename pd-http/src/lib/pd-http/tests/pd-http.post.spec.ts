import { PdHttpClient } from '../pd-http-client';
import {
  HttpTestingController,
  provideHttpClientTesting,
} from '@angular/common/http/testing';
import {
  HttpErrorResponse,
  HttpHeaders,
  provideHttpClient,
} from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

describe('PdHttpService#post tests', () => {
  let service: PdHttpClient;
  let controller: HttpTestingController;
  const URL = 'https://api.example.com/path/to/resource';

  const progressiveEvent = new ProgressEvent('Network error');

  const expectedErrorResponse = new HttpErrorResponse({
    error: progressiveEvent,
    status: 500,
    statusText: 'Server Error',
    url: URL,
  });

  const headers = new HttpHeaders();
  headers.append('Content-Type', 'application/json');
  headers.append('Authorization', 'Bearer token');

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });

    service = TestBed.inject(PdHttpClient);
    controller = TestBed.inject(HttpTestingController);
  });
});
