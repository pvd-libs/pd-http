import { TestBed } from '@angular/core/testing';
import { PdHttpClient } from '../pd-http-client';
import {
  HttpTestingController,
  provideHttpClientTesting,
} from '@angular/common/http/testing';
import {
  HttpErrorResponse,
  HttpEventType,
  HttpResponse,
  provideHttpClient,
} from '@angular/common/http';
import {
  ArrayBufferResponse,
  BlobResponse,
  CustomResponse,
  ObjectResponse,
  TextResponse,
} from './@spec-types';

describe('PdHttpService#delete tests', () => {
  let service: PdHttpClient;
  let controller: HttpTestingController;
  const URL = 'https://api.example.com/path/to/resource';

  const progressiveEvent = new ProgressEvent('Network error');

  const expectedErrorResponse = new HttpErrorResponse({
    error: progressiveEvent,
    status: 500,
    statusText: 'Server Error',
    url: URL,
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });

    service = TestBed.inject(PdHttpClient);
    controller = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    controller.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send a DELETE request to the provided URL and return an ArrayBuffer response', (done) => {
    const mockResponseData = new ArrayBuffer(8);

    service
      .delete(URL, { responseType: 'arraybuffer' })
      .subscribe((response) => {
        expect(response).toBe(mockResponseData);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData);
  });

  it('should send a DELETE request to the provided URL and throw error instead of return an ArrayBuffer response', (done) => {
    service.delete(URL, { responseType: 'arraybuffer' }).subscribe({
      error: (e: HttpErrorResponse) => {
        expect(e.status).toEqual(500);
        done();
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and returns An `Observable` of the response body as a `Blob`', () => {
    let res: Blob | null = null;

    service.delete(URL, { responseType: 'blob' }).subscribe((response) => {
      res = response;
    });

    const req = controller.expectOne(URL);

    const mockBlobData = new ArrayBuffer(8);
    const mockBlob = new Blob([mockBlobData], {
      type: 'application/octet-stream',
    });

    const reader = new FileReader();

    reader.onload = () => {
      const base64String = reader.result as string;

      req.flush(base64String);

      expect(req.request.method).toBe('DELETE');
      expect(res).toBeTruthy();

      const reader2 = new FileReader();
      reader2.onload = () => {
        const base64String2 = reader2.result as string;
        expect(base64String2).toBe(base64String);
      };
      reader2.readAsDataURL(res ?? new Blob());
    };

    reader.readAsDataURL(mockBlob);
  });

  it('should send a DELETE request to the provided URL should throw error instead of return An `Observable` of the response body as a `Blob`', () => {
    service.delete(URL, { responseType: 'blob' }).subscribe({
      error: (e: HttpErrorResponse) => {
        expect(e.status).toEqual(500);
        expect(e.statusText).toEqual('Server Error');
      },
    });

    const req = controller.expectOne(URL);
    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return a string response', (done) => {
    const mockResponseData = 'Sample response data';

    service.delete(URL, { responseType: 'text' }).subscribe((response) => {
      expect(response).toBe(mockResponseData);
      done();
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData);
  });

  it('should send a DELETE request to the provided URL and throw error instead of return a string response', (done) => {
    service.delete(URL, { responseType: 'text' }).subscribe({
      error: (e: HttpErrorResponse) => {
        expect(e.status).toEqual(500);
        done();
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the full event stream with response body as an ArrayBuffer', (done) => {
    const mockResponseData = new ArrayBuffer(8);

    service
      .delete(URL, { observe: 'events', responseType: 'arraybuffer' })
      .subscribe((event) => {
        if (event.type === HttpEventType.Response) {
          expect(event.body).toBe(mockResponseData);
          expect(event.status).toBe(200);
          expect(event.statusText).toBe('OK');
          done();
        }
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData, { status: 200, statusText: 'OK' });
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the full event stream with response body as an ArrayBuffer', () => {
    service
      .delete(URL, { observe: 'events', responseType: 'arraybuffer' })
      .subscribe({
        error: (e: HttpErrorResponse) => {
          expect(e.status).toEqual(500);
          expect(e.statusText).toEqual('Server Error');
        },
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the full event stream with response body as a Blob', (done) => {
    const mockResponseData = new Blob(['sample blob data'], {
      type: 'text/plain',
    });

    service
      .delete(URL, { observe: 'events', responseType: 'blob' })
      .subscribe((event) => {
        if (event.type === HttpEventType.Response) {
          const reader = new FileReader();
          reader.onload = () => {
            expect(reader.result).toBe('sample blob data');
            expect(event.status).toBe(200);
            expect(event.statusText).toBe('OK');
            done();
          };
          reader.readAsText(event.body ?? new Blob());
        }
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData, { status: 200, statusText: 'OK' });
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the full event stream with response body as a Blob', () => {
    service.delete(URL, { observe: 'events', responseType: 'blob' }).subscribe({
      error: (e: HttpErrorResponse) => {
        expect(e.status).toEqual(500);
        expect(e.statusText).toEqual('Server Error');
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the full event stream with response body as a string', (done) => {
    const mockResponseData = 'Sample response data';

    service
      .delete(URL, { observe: 'events', responseType: 'text' })
      .subscribe((event) => {
        if (event.type === HttpEventType.Response) {
          expect(event.body).toBe(mockResponseData);
          expect(event.status).toBe(200);
          expect(event.statusText).toBe('OK');
          done();
        }
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData, { status: 200, statusText: 'OK' });
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the full event stream with response body as a string', () => {
    service.delete(URL, { observe: 'events', responseType: 'text' }).subscribe({
      error: (e: HttpErrorResponse) => {
        expect(e.status).toEqual(500);
        expect(e.statusText).toEqual('Server Error');
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the full event stream with response body as an Object', (done) => {
    const mockResponseData = { key: 'value' };

    service
      .delete(URL, { observe: 'events', responseType: 'json' })
      .subscribe((event) => {
        if (event.type === HttpEventType.Response) {
          expect(event.body).toEqual(mockResponseData);
          expect(event.status).toBe(200);
          expect(event.statusText).toBe('OK');
          done();
        }
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData, { status: 200, statusText: 'OK' });
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the full event stream with response body as an Object', () => {
    service.delete(URL, { observe: 'events', responseType: 'json' }).subscribe({
      error: (e: HttpErrorResponse) => {
        expect(e.status).toEqual(500);
        expect(e.statusText).toEqual('Server Error');
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the full event stream with response body in the requested type', (done) => {
    const mockResponseData = { key: 'value' };

    service
      .delete(URL, { observe: 'events', responseType: 'json' })
      .subscribe((event) => {
        if (event.type === HttpEventType.Response) {
          expect(event.body).toEqual(mockResponseData);
          expect(event.status).toBe(200);
          expect(event.statusText).toBe('OK');
          done();
        }
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData, { status: 200, statusText: 'OK' });
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the full event stream with response body in the requested type', () => {
    service.delete(URL, { observe: 'events', responseType: 'json' }).subscribe({
      error: (e: HttpErrorResponse) => {
        expect(e.status).toEqual(500);
        expect(e.statusText).toEqual('Server Error');
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the full event stream with response body in the requested type', (done) => {
    const mockResponseData: { key: string; id: number } = {
      key: 'value',
      id: 1,
    };

    service
      .delete<{ key: string; id: number }>(URL, {
        observe: 'events',
        responseType: 'json',
      })
      .subscribe((event) => {
        if (event.type === HttpEventType.Response) {
          expect(event.body).toEqual(mockResponseData);
          expect(event.status).toBe(200);
          expect(event.statusText).toBe('OK');
          done();
        }
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData, { status: 200, statusText: 'OK' });
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the full event stream with response body in the requested type', () => {
    service
      .delete<{ key: string; id: number }>(URL, {
        observe: 'events',
        responseType: 'json',
      })
      .subscribe({
        error: (e: HttpErrorResponse) => {
          expect(e.status).toEqual(500);
          expect(e.statusText).toEqual('Server Error');
        },
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the full HttpResponse with response body as an ArrayBuffer', (done) => {
    const mockResponseData: ArrayBufferResponse = new ArrayBuffer(8);

    service
      .delete(URL, { observe: 'response', responseType: 'arraybuffer' })
      .subscribe((response) => {
        expect(response.body).toBe(mockResponseData);
        expect(response.status).toBe(200);
        expect(response.statusText).toBe('OK');
        expect(response).toBeInstanceOf(HttpResponse);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData, { status: 200, statusText: 'OK' });
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the full HttpResponse with response body as an ArrayBuffer', () => {
    service
      .delete(URL, { observe: 'response', responseType: 'arraybuffer' })
      .subscribe({
        error: (e: HttpErrorResponse) => {
          expect(e.status).toEqual(500);
          expect(e.statusText).toEqual('Server Error');
          expect(e.url).toEqual(URL);
          expect(e.error).toEqual(progressiveEvent);
        },
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the full HttpResponse with response body as a Blob', (done) => {
    const mockResponseData: BlobResponse = new Blob(['mock data'], {
      type: 'text/plain',
    });

    service
      .delete(URL, { observe: 'response', responseType: 'blob' })
      .subscribe((response) => {
        expect(response.body).toBe(mockResponseData);
        expect(response.status).toBe(200);
        expect(response.statusText).toBe('OK');
        expect(response).toBeInstanceOf(HttpResponse);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData, { status: 200, statusText: 'OK' });
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the full HttpResponse with response body as a Blob', () => {
    service
      .delete(URL, { observe: 'response', responseType: 'blob' })
      .subscribe({
        error: (e: HttpErrorResponse) => {
          expect(e.status).toEqual(500);
          expect(e.statusText).toEqual('Server Error');
          expect(e.url).toEqual(URL);
          expect(e.error).toEqual(progressiveEvent);
        },
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the full HttpResponse with response body as a string', (done) => {
    const mockResponseData: TextResponse = { key: 'mock response', id: 1 };

    service
      .delete(URL, { observe: 'response', responseType: 'text' })
      .subscribe((response) => {
        expect(response.body).toEqual(JSON.stringify(mockResponseData));
        expect(response.status).toBe(200);
        expect(response.statusText).toBe('OK');
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData, { status: 200, statusText: 'OK' });
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the full HttpResponse with response body as a string', () => {
    service
      .delete(URL, { observe: 'response', responseType: 'text' })
      .subscribe({
        error: (e: HttpErrorResponse) => {
          expect(e.status).toEqual(500);
          expect(e.statusText).toEqual('Server Error');
          expect(e.url).toEqual(URL);
          expect(e.error).toEqual(progressiveEvent);
        },
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the full HttpResponse with response body as an object', (done) => {
    const mockResponseData: ObjectResponse = {};

    service
      .delete(URL, { observe: 'response', responseType: 'json' })
      .subscribe((response) => {
        expect(response.body).toBe(mockResponseData);
        expect(response.status).toBe(200);
        expect(response.statusText).toBe('OK');
        expect(response).toBeInstanceOf(HttpResponse);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData, { status: 200, statusText: 'OK' });
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the full HttpResponse with response body as an object', () => {
    service
      .delete(URL, { observe: 'response', responseType: 'json' })
      .subscribe({
        error: (e: HttpErrorResponse) => {
          expect(e.status).toEqual(500);
          expect(e.statusText).toEqual('Server Error');
          expect(e.url).toEqual(URL);
          expect(e.error).toEqual(progressiveEvent);
        },
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the full HttpResponse with response body of the requested type', (done) => {
    const mockResponseData: CustomResponse = {
      id: 1,
      name: 'mock name',
    };

    service
      .delete<CustomResponse>(URL, {
        observe: 'response',
        responseType: 'json',
      })
      .subscribe((response) => {
        expect(response.body).toBe(mockResponseData);
        expect(response.status).toBe(200);
        expect(response.statusText).toBe('OK');
        expect(response).toBeInstanceOf(HttpResponse);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData, { status: 200, statusText: 'OK' });
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the full HttpResponse with response body of the requested type', () => {
    service
      .delete<CustomResponse>(URL, {
        observe: 'response',
        responseType: 'json',
      })
      .subscribe({
        error: (e: HttpErrorResponse) => {
          expect(e.status).toEqual(500);
          expect(e.statusText).toEqual('Server Error');
          expect(e.url).toEqual(URL);
          expect(e.error).toEqual(progressiveEvent);
        },
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the response body as an object parsed from JSON', (done) => {
    const mockResponseData = { key: 'value' }; // Replace with your mock response data

    service
      .delete(URL, { observe: 'body', responseType: 'json' })
      .subscribe((response) => {
        expect(response).toEqual(mockResponseData);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData);
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the response body as an object parsed from JSON', () => {
    service.delete(URL, { observe: 'body', responseType: 'json' }).subscribe({
      error: (e: HttpErrorResponse) => {
        expect(e.status).toEqual(500);
        expect(e.statusText).toEqual('Server Error');
        expect(e.url).toEqual(URL);
        expect(e.error).toEqual(progressiveEvent);
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should send a DELETE request to the provided URL and return the response body in the requested type', (done) => {
    const mockResponseData: CustomResponse = {
      id: 1,
      name: 'mock name',
    };

    service
      .delete<CustomResponse>(URL, { observe: 'body', responseType: 'json' })
      .subscribe((response) => {
        expect(response).toEqual(mockResponseData);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.flush(mockResponseData);
  });

  it('should send a DELETE request to the provided URL and throw error instead of return the response body in the requested type', () => {
    service
      .delete<CustomResponse>(URL, { observe: 'body', responseType: 'json' })
      .subscribe({
        error: (e: HttpErrorResponse) => {
          expect(e.status).toEqual(500);
          expect(e.statusText).toEqual('Server Error');
          expect(e.url).toEqual(URL);
          expect(e.error).toEqual(progressiveEvent);
        },
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toEqual('DELETE');

    req.error(progressiveEvent, expectedErrorResponse);
  });
});
