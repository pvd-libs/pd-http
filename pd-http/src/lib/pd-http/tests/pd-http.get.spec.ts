import { PdHttpClient } from '../pd-http-client';
import {
  HttpTestingController,
  provideHttpClientTesting,
} from '@angular/common/http/testing';
import {
  HttpErrorResponse,
  HttpEventType,
  provideHttpClient,
} from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { CustomResponse } from './@spec-types';
import { expectedErrorResponse, headers, progressiveEvent, URL } from './utils';

describe('PdHttpService#get tests', () => {
  let service: PdHttpClient;
  let controller: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });

    service = TestBed.inject(PdHttpClient);
    controller = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    controller.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should sent GET request and return response as An `Observable` of the response, with the response body as an `ArrayBuffer`', (done) => {
    const expectedResponse = new ArrayBuffer(8);
    service.get(URL, { responseType: 'arraybuffer' }).subscribe((response) => {
      expect(response).toBe(expectedResponse);
      done();
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse);
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of the response, with the response body as an `ArrayBuffer`', () => {
    service.get(URL, { responseType: 'arraybuffer' }).subscribe({
      error: (res: HttpErrorResponse) => {
        expect(res.error).toEqual(expectedErrorResponse);
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should sent GET request and return response as An `Observable` of the response, with the response body as an `Blob`', (done) => {
    const expectedResponse = new Blob();

    service.get(URL, { responseType: 'blob' }).subscribe((response) => {
      expect(response).toBe(expectedResponse);
      done();
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse);
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of the response, with the response body as an `Blob`', () => {
    service.get(URL, { responseType: 'blob' }).subscribe({
      error: (res: HttpErrorResponse) => {
        expect(res.error).toEqual(expectedErrorResponse);
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should sent GET request and return response as An `Observable` of the response, with the response body of type string', (done) => {
    const expectedResponse = { key: 'value', id: 1 };

    service.get(URL, { responseType: 'text' }).subscribe((response) => {
      expect(response).toEqual(JSON.stringify(expectedResponse));
      done();
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse);
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of the response, with the response body of type string', () => {
    service.get(URL, { responseType: 'text' }).subscribe({
      error: (res: HttpErrorResponse) => {
        expect(res.error).toEqual(expectedErrorResponse);
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should sent GET request and return response as An `Observable` of all `HttpEvent`s for the request, with the response body as an `ArrayBuffer`', (done) => {
    const expectedResponse = new ArrayBuffer(8);

    service
      .get(URL, { observe: 'events', responseType: 'arraybuffer' })
      .subscribe((event) => {
        if (event.type === HttpEventType.Response) {
          expect(event.body).toBe(expectedResponse);
          expect(event.status).toBe(200);
          expect(event.statusText).toBe('OK');
          expect(event.headers).toBe(headers);
          done();
        }
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse, { status: 200, statusText: 'OK', headers });
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of all `HttpEvent`s for the request, with the response body as an `ArrayBuffer`', () => {
    service
      .get(URL, { observe: 'events', responseType: 'arraybuffer' })
      .subscribe({
        error: (res: HttpErrorResponse) => {
          expect(res.error).toEqual(expectedErrorResponse);
        },
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should sent GET request and return response as An `Observable` of all `HttpEvent`s for the request, with the response body as an `Blob`', (done) => {
    const expectedResponse = new Blob();

    service
      .get(URL, { observe: 'events', responseType: 'blob' })
      .subscribe((event) => {
        if (event.type === HttpEventType.Response) {
          expect(event.body).toBe(expectedResponse);
          expect(event.status).toBe(200);
          expect(event.statusText).toBe('OK');
          expect(event.headers).toBe(headers);
          done();
        }
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse, { status: 200, statusText: 'OK', headers });
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of all `HttpEvent`s for the request, with the response body as an `Blob`', () => {
    service.get(URL, { observe: 'events', responseType: 'blob' }).subscribe({
      error: (res: HttpErrorResponse) => {
        expect(res.error).toEqual(expectedErrorResponse);
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should sent GET request and return response as An `Observable` of all `HttpEvent`s for the request, with the response body of type string', (done) => {
    const expectedResponse = { key: 'value', id: 1 };

    service
      .get(URL, { observe: 'events', responseType: 'text' })
      .subscribe((event) => {
        if (event.type === HttpEventType.Response) {
          expect(event.body).toEqual(JSON.stringify(expectedResponse));
          expect(event.status).toBe(200);
          expect(event.statusText).toBe('OK');
          expect(event.headers).toBe(headers);
          done();
        }
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse, { status: 200, statusText: 'OK', headers });
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of all `HttpEvent`s for the request, with the response body of type string', () => {
    service.get(URL, { observe: 'events', responseType: 'text' }).subscribe({
      error: (res: HttpErrorResponse) => {
        expect(res.error).toEqual(expectedErrorResponse);
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should sent GET request and return response as An `Observable` of the response, with the response body of type `Object`', (done) => {
    const expectedResponse = { key: 'value', id: 1 };

    service
      .get(URL, { observe: 'events', responseType: 'json' })
      .subscribe((response) => {
        if (response.type === HttpEventType.Response) {
          expect(response.body).toEqual(expectedResponse);
          expect(response.status).toBe(200);
          expect(response.statusText).toBe('OK');
          expect(response.headers).toBe(headers);
          done();
        }
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse, { status: 200, statusText: 'OK', headers });
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of the response, with the response body of type `Object`', () => {
    service.get(URL, { responseType: 'json' }).subscribe({
      error: (res: HttpErrorResponse) => {
        expect(res.error).toEqual(expectedErrorResponse);
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  // now test    * @return An `Observable` of the response, with a response body in the requested type.
  //    */
  //   get<T>

  it('should sent GET request and return response as An `Observable` of the response, with the response body of type `CustomResponse`', (done) => {
    const expectedResponse = { id: 1, name: 'name' };

    service
      .get<CustomResponse>(URL, { observe: 'events', responseType: 'json' })
      .subscribe((response) => {
        if (response.type === HttpEventType.Response) {
          expect(response.body).toEqual(expectedResponse);
          expect(response.status).toBe(200);
          expect(response.statusText).toBe('OK');
          expect(response.headers).toBe(headers);
          done();
        }
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse, { status: 200, statusText: 'OK', headers });
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of the response, with the response body of type `CustomResponse`', () => {
    service.get<CustomResponse>(URL, { responseType: 'json' }).subscribe({
      error: (res: HttpErrorResponse) => {
        expect(res.error).toEqual(expectedErrorResponse);
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should sent GET request and return response as An `Observable` of the `HttpResponse` for the request with the response body as an `ArrayBuffer`', (done) => {
    const expectedResponse = new ArrayBuffer(8);

    service
      .get(URL, { observe: 'response', responseType: 'arraybuffer' })
      .subscribe((response) => {
        expect(response.body).toBe(expectedResponse);
        expect(response.status).toBe(200);
        expect(response.statusText).toBe('OK');
        expect(response.headers).toBe(headers);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse, { status: 200, statusText: 'OK', headers });
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of the `HttpResponse` for the request with the response body as an `ArrayBuffer`', () => {
    service
      .get(URL, { observe: 'response', responseType: 'arraybuffer' })
      .subscribe({
        error: (res: HttpErrorResponse) => {
          expect(res.error).toEqual(expectedErrorResponse);
        },
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should sent GET request and return response as An `Observable` of the `HttpResponse` for the request with the response body as a `Blob`', (done) => {
    const expectedResponse = new Blob();

    service
      .get(URL, { observe: 'response', responseType: 'blob' })
      .subscribe((response) => {
        expect(response.body).toBe(expectedResponse);
        expect(response.status).toBe(200);
        expect(response.statusText).toBe('OK');
        expect(response.headers).toBe(headers);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse, { status: 200, statusText: 'OK', headers });
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of the `HttpResponse` for the request with the response body as a `Blob`', () => {
    service.get(URL, { observe: 'response', responseType: 'blob' }).subscribe({
      error: (res: HttpErrorResponse) => {
        expect(res.error).toEqual(expectedErrorResponse);
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  it('should sent GET request and return response as An `Observable` of the `HttpResponse` for the request with the response body of type string', (done) => {
    const expectedResponse = { key: 'value', id: 1 };

    service
      .get(URL, { observe: 'response', responseType: 'text' })
      .subscribe((response) => {
        expect(response.body).toEqual(JSON.stringify(expectedResponse));
        expect(response.status).toBe(200);
        expect(response.statusText).toBe('OK');
        expect(response.headers).toBe(headers);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse, { status: 200, statusText: 'OK', headers });
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of the `HttpResponse` for the request with the response body of type string', () => {
    service.get(URL, { observe: 'response', responseType: 'text' }).subscribe({
      error: (res: HttpErrorResponse) => {
        expect(res.error).toEqual(expectedErrorResponse);
      },
    });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });

  // now test    * @return An `Observable` of the `HttpResponse` for the request with a response body in the requested type.
  it('should sent GET request and return response as An `Observable` of the `HttpResponse` for the request with a response body in the requested type.', (done) => {
    const expectedResponse: CustomResponse = { id: 1, name: 'name' };

    service
      .get<CustomResponse>(URL, { observe: 'response', responseType: 'json' })
      .subscribe((response) => {
        expect(response.body).toEqual(expectedResponse);
        expect(response.status).toBe(200);
        expect(response.statusText).toBe('OK');
        expect(response.headers).toBe(headers);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse, { status: 200, statusText: 'OK', headers });
  });

  it('should sent GET request and return response as An `Observable` of the `HttpResponse` for the request with a response body in the requested type #2.', (done) => {
    const expectedResponse: Array<CustomResponse> = [
      { id: 1, name: 'name' },
      { id: 2, name: 'name2' },
    ];

    service
      .get<CustomResponse>(URL, { observe: 'response', responseType: 'json' })
      .subscribe((response) => {
        expect(response.body).toEqual(expectedResponse);
        expect(response.status).toBe(200);
        expect(response.statusText).toBe('OK');
        expect(response.headers).toBe(headers);
        done();
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.flush(expectedResponse, { status: 200, statusText: 'OK', headers });
  });

  it('should sent GET request and throw error instead of returning a response as An `Observable` of the `HttpResponse` for the request with a response body in the requested type.', () => {
    service
      .get<CustomResponse>(URL, { observe: 'response', responseType: 'json' })
      .subscribe({
        error: (res: HttpErrorResponse) => {
          expect(res.error).toEqual(expectedErrorResponse);
        },
      });

    const req = controller.expectOne(URL);

    expect(req.request.method).toBe('GET');

    req.error(progressiveEvent, expectedErrorResponse);
  });
});
