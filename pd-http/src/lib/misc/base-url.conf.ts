import { InjectionToken, Provider } from '@angular/core';

/**
 * @author Petar Dimitrov
 */

/**
 * This token is used to provide the base URL for the application.
 * It is used by the `UrlBuilder` to build URLs.
 */
export const BASE_URL = new InjectionToken<string | null>('BASE_URL', {
  providedIn: 'root',
  factory: () => {
    console.warn(
      'BASE_URL is not provided. Make sure to provide it in your application.'
    );
    return null;
  },
});

/**
 * This token is used to provide the authentication URL for the application.
 * It can be used from the UrlBuilder to build URLs if you are using 3rd-party auth providers.
 */
export const AUTH_URL = new InjectionToken<string | null>('AUTH_URL', {
  providedIn: 'root',
  factory: () => {
    return null;
  },
});

/**
 * This function can be used to provide the base URL for the application.
 * @param baseUrl
 * @returns
 */
export const provideBaseUrl = (baseUrl: string): Provider => {
  return { provide: BASE_URL, useValue: baseUrl };
};

/**
 * This function can be used to provide the authentication URL for the application.
 * @param authUrl
 * @returns
 */
export const provideAuthUrl = (authUrl: string): Provider => {
  return { provide: AUTH_URL, useValue: authUrl };
};
