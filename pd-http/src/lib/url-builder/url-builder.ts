import { Injectable, inject } from '@angular/core';
import { AUTH_URL, BASE_URL } from '../misc/base-url.conf';
import { UrlOptions } from '../misc/@types';

/**
 * @author Petar Dimitrov
 *
 * @description
 * The `UrlBuilder` is a service that provides methods to build URLs for the application.
 * It uses the base URL and the auth URL provided by the `BASE_URL` and `AUTH_URL` injection tokens.
 */
@Injectable({
  providedIn: 'root',
})
export class UrlBuilder {
  readonly #baseUrl = inject(BASE_URL);
  readonly #authUrl = inject(AUTH_URL, { optional: true });

  /**
   * @returns the base URL for the application.
   */
  get baseUrl(): string | null {
    return this.#baseUrl;
  }

  /**
   * @returns the auth URL for the application.
   */
  get authUrl(): string | null {
    return this.#authUrl;
  }

  /**
   *
   * @param path the path to append to the base URL
   * @returns the base URL for the application with the provided path appended.
   */
  buildUrl(path: string): string;

  /**
   *
   * @param path the path to append to the base URL
   * @param options the options to use when building the URL
   * @returns the base URL for the application with the provided path appended.
   *
   * @description In the optisons object, you can provide the following:
   *  - urlParams: an object with key-value pairs that will be used to replace placeholders in the path.
   *  - queryParams: an object with key-value pairs that will be used to build the query string.
   *  When you want the have URL builded with urlParams, you need to privde them in the path with the known JS syntax: path/${id}/path2/${cartId}.
   *
   * @example buildUrl('path/${id}/path2/${cartId}', { urlParams: { id: '1', cartId: '2' } });
   * @example buildUrl('path', { queryParams: { id: '1', cartId: '2' } });
   * @example buildUrl('path/${id}/path2/${cartId}', { urlParams: { id: '1', cartId: '2' }, queryParams: { id: '1', cartId: '2' } });
   */
  buildUrl(path: string, options: UrlOptions): string;
  buildUrl(path: string, options?: UrlOptions): string {
    if (options) {
      if (options.urlParams) {
        path = this.buildUrlParams(path, options.urlParams);
      }

      if (options.queryParams) {
        const query = this.buildQueryParams(options.queryParams);
        path = `${path}?${query}`;
      }
    }

    return `${this.#baseUrl}/${path}`;
  }

  /**
   *
   * @param path the path to append to auth base URL
   * @returns the auth URL for the application with the provided path appended.
   */
  buildAuthUrl(path: string): string;

  /**
   *
   * @param path the path to append to the base URL
   * @param options the options to use when building the URL
   * @returns the auth URL for the application with the provided path appended.
   *
   * @description In the optisons object, you can provide the following:
   *  - urlParams: an object with key-value pairs that will be used to replace placeholders in the path.
   *  - queryParams: an object with key-value pairs that will be used to build the query string.
   *  When you want the have URL builded with urlParams, you need to privde them in the path with the known JS syntax: path/${id}/path2/${cartId}.
   *
   * @example buildUrl('path/${id}/path2/${cartId}', { urlParams: { id: '1', cartId: '2' } });
   * @example buildUrl('path', { queryParams: { id: '1', cartId: '2' } });
   * @example buildUrl('path/${id}/path2/${cartId}', { urlParams: { id: '1', cartId: '2' }, queryParams: { id: '1', cartId: '2' } });
   */
  buildAuthUrl(path: string, options: UrlOptions): string;
  buildAuthUrl(path: string, options?: UrlOptions): string {
    if (!this.#authUrl) {
      throw new Error(
        'AUTH_URL is not provided. Make sure to provide it in your application.'
      );
    }

    if (options) {
      if (options.urlParams) {
        path = this.buildUrlParams(path, options.urlParams);
      }

      if (options.queryParams) {
        const query = this.buildQueryParams(options.queryParams);
        path = `${path}?${query}`;
      }
    }

    return `${this.#authUrl}/${path}`;
  }
  /**
   * @description This method is used to replace the placeholders in the path with the provided values.
   * @param path the path with placeholders
   * @param urlParams the object with key-value pairs that will be used to replace the placeholders.
   * @returns the path with the placeholders replaced with the provided values.
   * @example buildUrlParams('path/${id}/path2/${cartId}', { id: '1', cartId: '2' });
   */
  private buildUrlParams(
    path: string,
    urlParams: Record<string, string>
  ): string {
    Object.keys(urlParams).forEach((key) => {
      path = path.replace(new RegExp(`\\$\\{${key}\\}`, 'g'), urlParams[key]);
    });

    return path;
  }

  /**
   * @description This method is used to build the query string from the provided query params.
   * @param queryParams the object with key-value pairs that will be used to build the query string.
   * @returns the query string.
   * @example buildQueryParams({ id: '1', cartId: '2' });
   */
  private buildQueryParams(queryParams: Record<string, string>): string {
    return Object.keys(queryParams)
      .map((key) => `${key}=${queryParams[key]}`)
      .join('&');
  }
}
